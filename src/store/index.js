import Vuex from "vuex";

import loadPosts from "./modules/loadPosts";
import loadCurrentPost from "./modules/loadCurrentPost";
import loadCategory from "./modules/loadCategory";
import loadCurrentCatPosts from "./modules/loadCurrentCatPosts";

export default new Vuex.Store({
  modules: {
    loadPosts,
    loadCurrentPost,
    loadCategory,
    loadCurrentCatPosts
  },
});
