import axios from "axios";

const state = {
    currentCatPost: [],
};

const getters = {
    loadCurrentCatPost: (state) => state.currentCatPost,
};

const actions = {
    async fetchCurrentCatPost({ commit }) {
        let regEx = /\/cat-posts\/\d+/g;
        let url = window.location.href.match(regEx);
        let part = url[0].split("/");
        let catId = part[part.length - 1];
        let data = [];
        let result = (
            await axios.get(
                `https://blog.ted.com/wp-json/wp/v2/posts?_fields=id,title,categories,content&per_page=100&categories=${catId}`
            )
        ).data;
        data = data.concat(result);
        commit("loadCurrentCatPost", data);
    },
};

const mutations = {
    loadCurrentCatPost: (state, currentCatPost) => (state.currentCatPost = currentCatPost),
};

export default {
    state,
    getters,
    actions,
    mutations,
};
