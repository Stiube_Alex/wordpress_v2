import axios from "axios";

const state = {
    currentPost: [],
};

const getters = {
    loadCurrentPost: (state) => state.currentPost,
};

const actions = {
    async fetchCurrentPost({ commit }) {
        let page = 1;
        let regEx = /\/postPage\/\d+/g;
        let url = window.location.href.match(regEx);
        let part = url[0].split("/");
        let postId = part[part.length - 1];
        let data = [];
        let result = (
            await axios.get(
                `https://blog.ted.com/wp-json/wp/v2/posts?_fields=id,title,categories,content&page=${page}&include=${postId}`
            )
        ).data;
        data = data.concat(result);
        commit("loadCurrentPost", data);
    },
};

const mutations = {
    loadCurrentPost: (state, currentPost) => (state.currentPost = currentPost),
};

export default {
    state,
    getters,
    actions,
    mutations,
};
