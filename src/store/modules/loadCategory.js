import axios from "axios";

const state = {
    categoryLoad: [],
};

const getters = {
    loadCategory: (state) => state.categoryLoad,
};

const actions = {
    async fetchCategory({ commit }) {
        let data = [];
        let result = (
            await axios.get(
                `https://blog.ted.com/wp-json/wp/v2/categories?_fields=name,id&per_page=100`
            )
        ).data;
        data = data.concat(result);
        commit("loadCategory", data);
    },
};

const mutations = {
    loadCategory: (state, categoryLoad) => (state.categoryLoad = categoryLoad),
};

export default {
    state,
    getters,
    actions,
    mutations,
};
