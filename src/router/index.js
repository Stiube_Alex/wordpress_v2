import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../views/Home.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/page/:page/",
    name: "PageCounter",
    component: () => import("../views/PageCounter.vue"),
  },
  {
    path: "/about",
    name: "About",
    component: () => import("../views/About.vue"),
  },
  {
    path: "/postPage/:id/",
    name: "PostPage",
    component: () => import("../views/PostPage.vue"),
  },
  {
    path: "/category",
    name: "CategoryPage",
    component: () => import("../views/CategoryPage.vue"),
  },
  {
    path: "/category/cat-posts/:id/:name/",
    name: "CategoryPosts",
    component: () => import("../views/CategoryPosts.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
